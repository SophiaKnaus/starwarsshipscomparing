
import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import * as actions from '../actions/actions';

const starshipsStateReducer = handleActions({
    [actions.getAllStarshipsSuccess](state, {payload}) {
        return payload.starships;
    }
}, []);

const starshipComparisonStateReducer = handleActions({
    [actions.addStarshipToComparison](state, {payload}) {
        const { starshipId } = payload;

        if (state.includes(starshipId)) {
            return state;
        }

        return [...state, starshipId];
    },
    
    [actions.deleteStarshipToComparison](state, {payload}) {
        const { starshipId } = payload;
        const indexId = state.indexOf(starshipId);
        state.splice(indexId,1);
        return [...state];
    }

}, [])

export default combineReducers({
    starships: starshipsStateReducer,
    starshipsComparison: starshipComparisonStateReducer,
});