import classes from'./MainPage.module.css';
import React, {useCallback, useEffect, useState} from 'react';
import Cards from './Cards/Cards';
import Search from './Components/Search';
import { connect } from 'react-redux';
import Button from './Components/Button';
import * as actions from './actions/actions';
import { useHistory } from 'react-router';
import PageTitle from './Components/PageTitle';

const mapStateToProps = (state) => {
  return {
    starships: state.starships,
    starshipsComparison: state.starshipsComparison,
  }
}

const actionsCreators = {
  addStarshipToComparison: actions.addStarshipToComparison,
}

const MainPage = ({starships, starshipsComparison, addStarshipToComparison}) => {

    const [searchString, setSearchString] = useState('');
    const [buttonVisible, setButtonVisible] = useState(false);

    const filteredStarships = starships.filter(item => {
      return item.name.toLowerCase().includes(
        searchString.toLowerCase()
      );
    })

    const onCompareClickHandler = useCallback((starshipId) => {
      addStarshipToComparison({ starshipId });
    }, [addStarshipToComparison]);

    useEffect(() => {
      if (starshipsComparison.length) {
        setButtonVisible(true);
      }
    },[starshipsComparison]);

    const history = useHistory();

    const onGoToComparison = useCallback(() => {
        history.push('/StarshipsComparison');
    },[history])
    
      return (
        <div className={classes.MainPage}>
          <PageTitle text='StarWarsStarships'/>
          <Search value={searchString} onChange={setSearchString} />
          <Cards starships={filteredStarships} onCompareClick={onCompareClickHandler} />
          <div className={classes.Button}>
            {buttonVisible && <Button text='Перейти к сравнению' onClick={onGoToComparison} />}
          </div>
        </div>
      );
}

export default connect(mapStateToProps, actionsCreators)(MainPage);