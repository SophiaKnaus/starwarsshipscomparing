const parseNotNormalizedFloat = (value) => {
    const normalizedValue = value.replace(/,/g, '');
    return parseFloat(normalizedValue);
};

export const defaultNumberParser = (value) => ({
    valid: true,
    parsedValue: parseNotNormalizedFloat(value),
});

const unitToWeightMap = {
    "year": 365,
    "month": 30,
    "week": 7,
    "day": 1,
};

export const consumablesParser = (value) => {
    const [count, unit] = value.split(' ');
    const normalizedUnit = unit.endsWith('s') ? unit.substring(0, unit.length - 1) : unit;

    return {
        valid: true,
        parsedValue: parseNotNormalizedFloat(count) * unitToWeightMap[normalizedUnit],
    }
};

export const costParser = (value) => value === "unknown" ? {
    valid: false
} : {
    valid: true,
    parsedValue: parseNotNormalizedFloat(value)
};

export const passengersParser = (value) => value === "n/a" ? {
    valid: false
} : {
    valid: true,
    parsedValue: parseNotNormalizedFloat(value)
};