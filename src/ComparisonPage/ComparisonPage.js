import React, { useCallback, useEffect } from 'react';
import classes from './ComparisonPage.module.css';
import { connect } from 'react-redux';
import StarshipsOptions from '../Components/StarshipsOptions';
import cn from 'classnames';
import PageTitle from '../Components/PageTitle';
import { Link } from 'react-router-dom';
import * as actions from '../actions/actions';
import ComparisonImage from '../ComparisonImage/ComparisonImage';
import { defaultNumberParser, consumablesParser, costParser, passengersParser } from "./comparisonParsers";
import { useHistory } from 'react-router';


const mapStateToProps = (state) => {
    return {
        starships: state.starships,
        starshipsComparison: state.starshipsComparison,
    };
};

const actionsCreator = {
    deleteStarshipToComparison: actions.deleteStarshipToComparison,
}

const ComparisonPage = ({starships, starshipsComparison, deleteStarshipToComparison }) => {

    const history = useHistory();

    useEffect(() => {
        if(!starshipsComparison.length) {
            history.push('/StarWarsStarships');
        }
    },[starshipsComparison, history])

    const onDeleteButtonClickHandler = useCallback((starshipId) => {
        deleteStarshipToComparison({starshipId});
    },[deleteStarshipToComparison]);

    const starshipsForComparisone = starships.filter(item => starshipsComparison.includes(item.id));
    const names = starshipsForComparisone.map(item => item.name);
    const models = starshipsForComparisone.map(item => item.model);
    const manufacturers = starshipsForComparisone.map(item => item.manufacturer);
    const costs = starshipsForComparisone.map(item => item.cost_in_credits);
    const lengths = starshipsForComparisone.map(item => item.length);
    const maxSpeeds = starshipsForComparisone.map(item => item.max_atmosphering_speed);
    const crews = starshipsForComparisone.map(item => item.crew);
    const  passengers = starshipsForComparisone.map(item => item.passengers);
    const cargo_capacity = starshipsForComparisone.map(item => item.cargo_capacity);
    const consumables = starshipsForComparisone.map(item => item.consumables);
    const hyperdrive_ratings = starshipsForComparisone.map(item => item.hyperdrive_rating);
    const MGLTs = starshipsForComparisone.map(item => item.MGLT);
    const imgs = starshipsForComparisone.map(item => ({
        src: item.imgSrc,
        id: item.id,
    }));

    return (
        <div className={classes.ComparisonPage}>
            <PageTitle text='StarWarsStarshipComparison'/>
            <div className={classes.ImagesWrapper}>
                <div className={cn(classes.OptionWrapper, classes.StarshipImgWrapper)}/>
                {imgs.map(imgProps => (
                    <ComparisonImage {...imgProps} className={classes.OptionWrapper} onClick={onDeleteButtonClickHandler} />
                ))}
            </div>
            <StarshipsOptions className={classes.OptionWrapper} title={'Название:'} values={names} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Модель:'} values={models} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Производитель:'} values={manufacturers} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Цена(кредитов):'} values={costs} parseToCompare={costParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Длина(м):'} values={lengths} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Максимальная скорость в атмосфере(км/ч):'} values={maxSpeeds} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Мегасвет(МгС):'} values={MGLTs} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Рейтинг гипердвигателя(класс):'} values={hyperdrive_ratings} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Грузоподъемность(тонн):'} values={cargo_capacity} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Автономность:'} values={consumables} parseToCompare={consumablesParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Команда(человек):'} values={crews} parseToCompare={defaultNumberParser} />
            <StarshipsOptions className={classes.OptionWrapper} title={'Пасажиры(человек):'} values={passengers} parseToCompare={passengersParser} />
            <Link className={classes.Navigation} to='/StarWarsStarships'>Вернуться на главную</Link>    
        </div>
    );
};

export default connect(mapStateToProps, actionsCreator)(ComparisonPage);