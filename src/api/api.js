import axios from "axios";

const instanse = axios.create({
    baseURL: 'https://swapi.dev/api/',
});

 const getDataByUrl = async (url, params = {}) => {
    const respose = await instanse.get(`${url}`, {
        params: {
            ...params,
            format: "json",
        },
    });

    return respose.data;
};

export const getAllStarships = async() => {
    let result = await getDataByUrl('/starships', {
        page: 1,
    });
    const arrStarships  = result.results;

    while (result.next) {
        result = await getDataByUrl(result.next);
        arrStarships.push(...result.results);
    }

    return arrStarships;
}

export const getAllFilms = async() => {
    let result = await getDataByUrl('/films');
    return result.results;
};