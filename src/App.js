import classes from'./App.module.css';
import React, {useEffect} from 'react';
import * as actions from './actions/actions';
import { connect } from 'react-redux';
import MainPage from './MainPage';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import ComparisonPage from './ComparisonPage/ComparisonPage';


const actionsCreators = {
  getStarships: actions.getStarships,
}

function App({starships, getStarships}) {

  useEffect(() => {
    getStarships();
  }, [getStarships]);


  return (
    <div className={classes.App}>
      <Router>
        <Switch>
          <Route path='/StarWarsStarships'>
            <MainPage/>
          </Route>
          <Route path='/StarshipsComparison'>
            <ComparisonPage/>
          </Route>
          <Route path='/'>
            <Redirect to='/StarWarsStarships' />
          </Route>
        </Switch>
      </Router>
      
    </div>
    
  );
}

export default connect(null,actionsCreators)(App);
