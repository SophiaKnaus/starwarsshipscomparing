import { createAction } from 'redux-actions';
import { getAllStarships, getAllFilms } from '../api/api';
import _ from 'lodash';
import executorImg from '../pictures/Executor.png';
import firesprayImg from '../pictures/Firespray.png';
import jr75Img from '../pictures/GR-75.png';
import imperialShittle from '../pictures/ImperialShuttle.png';
import nebulon from '../pictures/Nebulon.png';
import milenniumFalcon from '../pictures/Millennium_Falcon.png';
import xWing from '../pictures/X-wing_Fathead.png';
import yWing from '../pictures/YWing_NEGVV.png';
import starDestroyer from '../pictures/ImperialClassStarDestroyer-TSWB.png';

const  typeToImg = {
    'Star Destroyer': starDestroyer,
    'Millennium Falcon': milenniumFalcon,
    'Y-wing': yWing,
    'X-wing': xWing,
    'Executor': executorImg,
    'Rebel transport': jr75Img,
    'Slave 1': firesprayImg,
    'Imperial shuttle': imperialShittle,
    'EF76 Nebulon-B escort frigate': nebulon,

};

export const getAllStarshipsSuccess = createAction('GET_ALL_STARSHIPS_SUCCESS');
export const getAllStarshipsFailure = createAction('GET_ALL_STARSHIPS_FAILURE');
export const getAllStarshipsReguest = createAction('GET_ALL_STARSHIPS_REQUEST');

export const getStarships = () => async (dispatch) => {

    dispatch(getAllStarshipsReguest);
    try {
        const [responseStarships, responseFilms] = await Promise.all([
            getAllStarships(),
            getAllFilms(),
        ]);
        const episodeV = responseFilms.find(film => film.episode_id === 5);
        const episodeVUrl = episodeV.url;
        const sortedStarshipsWithIds = responseStarships
            .filter(starship => starship.films.includes(episodeVUrl))
            .map(starship => ({
                ...starship, 
                id: _.uniqueId(),
                imgSrc: typeToImg[starship.name], 
            }));
        dispatch(getAllStarshipsSuccess({starships: sortedStarshipsWithIds}));
    } catch(e) {
        dispatch(getAllStarshipsFailure());
    }

};

export const addStarshipToComparison = createAction('ADD_STARSHIP_TO_COMPARISON');
export const deleteStarshipToComparison = createAction('DELETE_STARSHIP_TO_COMPARISON');

