import React from 'react';
import classes from './StarshipImage.module.css';
import cn from 'classnames';

const StarshipImage = ({imageSrc, className}) => {
    return (
        <img className={cn(classes.Img, className)} src={imageSrc} alt='starship'></img>
    );
};

export default StarshipImage;