import React from 'react';
import classes from './StarshipsOptions.module.css';
import Span from '../Components/Span';
import cn from 'classnames';

const StarshipsOptions = ({title, values, unit, className, parseToCompare}) => {
    const maxValue = parseToCompare && Math.max(...values.map(parseToCompare).filter(item => item.valid).map(item => item.parsedValue));
    const minValue = parseToCompare && Math.min(...values.map(parseToCompare).filter(item => item.valid).map(item => item.parsedValue));

    return (
        <div className={classes.StarshipsOptions}>
            <Span text={title} className={className} />
            {values.map((value, idx) => {
                const parsedValueResult = parseToCompare ? parseToCompare(value) : { valid: false };
                const isMax = parsedValueResult.valid && parsedValueResult.parsedValue === maxValue;
                const isMin = parsedValueResult.valid && parsedValueResult.parsedValue === minValue;

                return (
                    <Span
                        key={idx}
                        text={[value, unit || ''].join(' ')}
                        className={cn(
                            classes.StarshipsOptionsValue,
                            {
                                [classes.CriticalValue]: isMax || isMin,
                                [classes.MaxValue]: isMax,
                                [classes.MinValue]: isMin,
                            },
                            className
                        )}
                    /> 
                );
            })} 
        </div>
    );
};

export default StarshipsOptions;