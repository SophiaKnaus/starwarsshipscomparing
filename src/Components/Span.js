import React from "react";
import classes from './Span.module.css';
import cn from 'classnames';

const Span = ({text, className}) => {
    const spanClasses = cn(classes.Span, className)
    return (
        <span 
            className={spanClasses}
        >
            {text}
        </span>
    )
};

export default Span;