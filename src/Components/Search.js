import React, { useCallback } from 'react';
import classes from './Search.module.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const Search = ({value, onChange}) => {

    const ChangeHandler = useCallback((evt) => {
        onChange(evt.target.value);
    },[onChange])

    return (
        <div className={classes.Container}>
            <input 
                className={classes.Input}
                value={value}
                onChange={ChangeHandler}
            >
            </input>
            <FontAwesomeIcon size='2x' icon={faSearch} />
        </div>

    );
};

export default Search;