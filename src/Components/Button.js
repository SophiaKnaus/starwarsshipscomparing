import React from 'react';
import classes from './Button.module.css';
import cn from 'classnames';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

const iconToImageMap = {
    delete: faTimesCircle,
}


const Button = ({icon, onClick, text}) => {
    const ButtonClass = cn({[classes.ButtonText] : text}, {[classes.ButtonIcon] : icon});
    const srcIcon = iconToImageMap[icon];

    return (
        <button 
            className={ButtonClass}
            onClick={onClick}
        >
            <FontAwesomeIcon size='2x' icon={srcIcon}/> {text}
        </button>
    );
}

export default Button;
