import React from 'react';
import classes from './PageTitle.module.css';

const PageTitle = ({text}) => {
    return (
        <div className={classes.PageTitle}>
            <h1 className={classes.Text}>{text}</h1>
        </div>
    );
};

export default PageTitle;