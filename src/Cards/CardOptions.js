import React from 'react';
import classes from './CardOptions.module.css';
import Span from '../Components/Span';

const CardOptions = ({title, value}) => {
    return (
        <div className={classes.CardOptions}>
            <Span text={title} />
            <Span text={value} className={classes.CardOptionsValue} />
        </div>
    );
};

export default CardOptions;