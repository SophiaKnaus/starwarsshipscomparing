import React from 'react';
import classes from './Cards.module.css';
import Card from './Card';

const Cards = ({starships, onCompareClick}) => {
    console.log(starships);

    return (
        <div className={classes.Cards}>
          {starships.map((item) => <Card key={item.id} starship={item} onCompareClick={onCompareClick} />)}
        </div>
    );
};

export default Cards;