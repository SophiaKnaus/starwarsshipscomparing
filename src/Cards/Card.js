import React, { useCallback } from 'react';
import classes from './Card.module.css';
import Button from '../Components/Button';
import StarshipImage from '../Components/StarshipImage';
import StarshipsOptions from '../Components/StarshipsOptions';

const Card = ({starship, onCompareClick}) => {
    const compareHandler = useCallback(() => {
        onCompareClick(starship.id);
    }, [onCompareClick, starship.id]);

    return (
        <div className={classes.Card}>
            <div className={classes.StarshipImg}>
                <StarshipImage imageSrc={starship.imgSrc}/>
            </div>
            <div className={classes.Name}>
                <h2 className={classes.Text}>{starship.name}</h2>
            </div>
            <div className={classes.Options}>
                <div className={classes.Container}>
                    <StarshipsOptions title={`Название:`} values={[starship.name]}/>
                    <StarshipsOptions title={`Модель:`} values={[starship.model]}/>
                    <StarshipsOptions title={`Производитель:`} values={[starship.manufacturer]}/>
                    <StarshipsOptions title={`Цена:`} values={[starship.cost_in_credits]} unit='кредитов'/>
                    <StarshipsOptions title={`Длина:`} values={[starship.length]} unit='метров'/>
                    <StarshipsOptions title={`Максимальная скорость в атмосфере:`} values={[starship.max_atmosphering_speed]} unit='км/ч' />
                    <StarshipsOptions title={`Мегасвет:`} values={[starship.MGLT]} unit='МгС' />
                    <StarshipsOptions title={`Рейтинг гипердвигателя:`} values={[starship.hyperdrive_rating]} unit='класс'/>
                    <StarshipsOptions title={`Грузоподъемность:`} values={[starship.cargo_capacity]} unit='тонн' />
                    <StarshipsOptions title={`Автономность:`} values={[starship.consumables]}/>
                    <StarshipsOptions title={`Команда:`} values={[starship.crew]} unit='человек' />
                    <StarshipsOptions title={`Пасажиры:`} values={[starship.passengers]} unit='человек' />
                </div>
            </div>
            <div className={classes.ButtonContainer}>
                <Button text={'Сравнить'} onClick={compareHandler}/>
            </div>
        </div>
    );
};

export default Card;