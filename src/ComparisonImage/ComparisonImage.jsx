import React, { useCallback } from 'react';
import classes from './ComparisonImage.module.css';
import StarshipImage from '../Components/StarshipImage';
import cn from 'classnames';
import Button from '../Components/Button';

const ComparisonImage = ({ id, src, onClick, className }) => {
    const handleDeleteClick = useCallback(() => {
        onClick(id);
    }, [onClick, id]);

    return (
        <div className={cn(classes.StarshipImgWrapper, className)}>
            <div className={classes.Button}>
                <Button icon='delete' onClick={handleDeleteClick} />
            </div>
            <StarshipImage imageSrc={src} className={classes.StarshipImg} />
        </div>
    );
};

export default ComparisonImage;